import React from 'react';
import classes from './Footer.module.css';
import { NavLink } from 'react-router-dom';
import NavBar from '../NavBar/NavBar';

const Footer = () => {
	return (
		<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
	        <NavLink className="navbar-brand" to="/">
				<img className={classes.logo} src="/img/logo.jpg" />
	        </NavLink>
		  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavFooter" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span className="navbar-toggler-icon"></span>
		  </button>
		  <div className="collapse navbar-collapse" id="navbarNavFooter">
		    <NavBar />
		  </div>
		</nav>

		);
}

export default Footer;