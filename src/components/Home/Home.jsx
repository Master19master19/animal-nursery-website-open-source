import React from 'react';
import classes from './Home.module.css';
import SlideShow from '../SlideShow/SlideShow';
import { NavLink } from 'react-router-dom';

const Home = ( props ) => {
	let pups = props.puppies.map( ( p ) =>
		<div className="col-md-4 text-center">
			<div className={classes.thumbnailParent}>
				<img src={p.image} className={ `img-thumbnail ${classes.thumbnail}` } />
				<p>{p.name}</p>
				<NavLink className="nav-link" to={ `/pet/${p.id}` }>
		        	Read more
		        </NavLink>
			</div>
		</div>
	);
	return (
	<div>
		
		<div>
			<SlideShow />
		</div>
		<div>
			<h3 className="text-center">
				Find your perfect companion
			</h3>
			<div className="row d-flex justify-content-center">
				{pups}
			</div>
		</div>
	</div>
		);
}

export default Home;