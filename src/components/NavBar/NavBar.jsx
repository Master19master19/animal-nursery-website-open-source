import React from 'react';
import classes from './NavBar.module.css';
import { NavLink } from 'react-router-dom';

const NavBar = () => {
	return (<ul className="navbar-nav">
		      <li className="nav-item">
		        <NavLink activeClassName={ "alternative-active-class" } className="nav-link" to="/">
		        	Home
		        </NavLink>
		      </li>
		      <li className="nav-item">
		        <NavLink className="nav-link" to="/dog">
		        	Dogs
		        </NavLink>
		      </li>
		      <li className="nav-item">
		        <NavLink className="nav-link" to="/cat">
		        	Cats
		        </NavLink>
		      </li>
		      <li className="nav-item">
		        <NavLink className="nav-link" to="/other">
		        	Other
		        </NavLink>
		      </li>
		      <li className="nav-item">
		        <NavLink className="nav-link" to="/about">
		        	About us
		        </NavLink>
		      </li>
		      <li className="nav-item">
		        <NavLink className="nav-link" to="/contact">
		        	Contact us
		        </NavLink>
		      </li>
		    </ul>
		);
}

export default NavBar;