import React from 'react';
import classes from './Post.module.css';

const Post = ( props ) => {
	return (
		<div>
			<div className="row">
				<div className="col-3">
					<img className={`img-thumbnail ${classes.thumb}`} src="https://image.shutterstock.com/image-photo/beautiful-water-drop-on-dandelion-260nw-789676552.jpg" />
				</div>
				<div className="col-7">
					<p>{props.title}</p>
				</div>
			</div>
			<div>
				<button className="btn btn-sm btn-primary">Like</button>
			</div>
		</div>
	);
}

export default Post;