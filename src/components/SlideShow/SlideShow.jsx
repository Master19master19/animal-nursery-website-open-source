import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import classes from './SlideShow.module.css';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

const slideImages = [
  'https://www.klevermedia.co.uk/html_templates/petcare/images/3.png',
  'https://www.klevermedia.co.uk/html_templates/petcare/images/2.png',
];
class SlideShow extends Component {
    render() {
        return (
            <Carousel autoPlay="true" infiniteLoop="true" className={ `w-100` }>
                <div className={classes.maxHeight}>
                    <img src="https://live.staticflickr.com/5568/14938727197_04c2bd7ba6_b.jpg" />
                    <p className="legend">Some text</p>
                </div>
                <div className={classes.maxHeight}>
                    <img src="https://cdn.vox-cdn.com/thumbor/wng90rt7pFT3o_oPRNV21iK-2x8=/0x0:4560x3041/1200x800/filters:focal(1916x1157:2644x1885)/cdn.vox-cdn.com/uploads/chorus_image/image/58504395/911428568.jpg.0.jpg" />
                </div>
                <div className={classes.maxHeight}>
                    <img src="https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12193133/German-Shepherd-Puppy-Fetch.jpg" />
                </div>
            </Carousel>
        );
    }
}

export default SlideShow;