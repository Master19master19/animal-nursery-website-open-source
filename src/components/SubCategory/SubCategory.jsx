import React from 'react';
import classes from './SubCategory.module.css';

const SubCategory = () => {
    return (
        <div className={classes.content}>
            <h1>SubCategory</h1>
        </div>
        );
}

export default SubCategory;