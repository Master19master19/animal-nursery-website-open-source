import React from 'react';
import classes from './Pet.module.css';
import SlideShow from '../SlideShow/SlideShow';

let glob = {
  homeProps: {
      puppies: [
      {id:1,name:'Suzan',image:'https://cdn.pixabay.com/photo/2016/12/13/05/15/puppy-1903313__340.jpg'},
      {id:2,name:'Dawg',image:'https://scx2.b-cdn.net/gfx/news/hires/2018/2-dog.jpg'},
      {id:3,name:'Rex',image:'https://www.dogster.com/wp-content/uploads/2018/09/Carolina-Dog.jpg'},
    ]
  }
}

const Pet = ( props ) => {
	let dog = glob.homeProps.puppies[ props.match.params.id ];
	return (
		<div className="container">
			<h1 class="my-md-4 my-2 text-center">{dog.name}</h1>
			<div class="row">
				<div class="col-md-6">
					<SlideShow />
				</div>
				<div class="col-md-6">
					<p>
						Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum.
					</p>
					<ol className={classes.list}>
						<li><span>Age</span> 4 years</li>
						<li><span>Size</span> Small</li>
						<li><span>Breed</span> West highland terrier</li>
						<li><span>Gender</span> Female</li>
						<li><span>Spayed</span> No</li>
						<li><span>Live with cats</span> Yes</li>
						<li><span>Home tested</span> Yes</li>
						<li><span>Child friendly</span> Yes</li>
						<li><span>Reference</span> 19475638</li>
					</ol>
				</div>
			</div>
			<div>
				<h3 className="centered-title">
					<span>We have more dogs in need</span>
				</h3>
				<p>
					Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.
				</p>
			</div>
		</div>
	);
}

export default Pet;