import React from 'react';
import classes from './Dialogs.module.css';
import { NavLink } from 'react-router-dom';

const DialogItem = ( props ) => {
	return (
	<li class="list-group-item">
		<NavLink to={ `/dialogs/${props.id}` } activeClassName={classes.active}>{props.name}</NavLink>
	</li>
	);
}

const Dialogs = ( props ) => {
	return (
		<div className="row mt-4">
			<div className="col-4 col-md-3">
				<ul class="list-group list-group-flush">
			  		<DialogItem id="1" name="User 1" />
			  		<DialogItem id="2" name="User 2" />
			  		<DialogItem id="3" name="User 3" />
				</ul>
			</div>
			<div className="col-8 pt-5">
				<div>
					<p>Hi , this is a test message</p>
				</div>
			</div>
		</div>
	);
}

export default Dialogs;