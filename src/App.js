import React from 'react';
import './App.css';
import { Route } from 'react-router-dom';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Pet from './components/Pet/Pet';
import Home from './components/Home/Home';
import Contact from './components/Contact/Contact';
import Category from './components/Category/Category';
import SubCategory from './components/SubCategory/SubCategory';
import About from './components/About/About';
import Dialogs from './components/Dialogs/Dialogs';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';

let glob = {
  homeProps: {
      puppies: [
      {id:1,name:'Suzan',image:'https://cdn.pixabay.com/photo/2016/12/13/05/15/puppy-1903313__340.jpg'},
      {id:2,name:'Dawg',image:'https://scx2.b-cdn.net/gfx/news/hires/2018/2-dog.jpg'},
      {id:3,name:'Rex',image:'https://www.dogster.com/wp-content/uploads/2018/09/Carolina-Dog.jpg'},
    ]
  }
}

const App = () => {
  return (
    <div className="">
      <div>
        <Header />
        <div>
          <Route path="/" exact render={(props) => <Home { ...glob.homeProps } /> } />
          <Route path="/about" component={About} />
          <Route path="/contact" component={Contact} />
          <Route path="/dog" exact render={(props) => <Category { ...glob.homeProps } /> } />
          <Route path="/cat" exact render={(props) => <Category { ...glob.homeProps } /> } />
          <Route path="/other" exact render={(props) => <Category { ...glob.homeProps } /> } />
          <Route path="/dog/:id" render={(props) => <SubCategory { ...glob.homeProps } /> } />
          <Route path="/cat/:id" render={(props) => <SubCategory { ...glob.homeProps } /> } />
          <Route path="/other/:id" render={(props) => <SubCategory { ...glob.homeProps } /> } />
          <Route path="/pet/:id" render={(props) => <Pet {...props} />}/> 
        </div>
        <Footer />
      </div>
    </div>
  );
}

export default App;
